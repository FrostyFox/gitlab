const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    video: true,
    // specPattern: "cypress/e2e/homework04.cy.js",
       supportFile: false,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
  reporter: 'mochawesome',
  reporterOptions: {
    reportDir: 'cypress/results',
    overwrite: false,
    html: true,
    json: false,
  },
});